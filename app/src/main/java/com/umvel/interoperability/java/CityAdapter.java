package com.umvel.interoperability.java;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.umvel.interoperability.R;
import com.umvel.interoperability.java.model.CityModel;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ContactViewHolder> {

    List<CityModel> infoCity;


    public int position = 0;

    public void addSomeCities(int... items){
        if(infoCity.isEmpty())
            return;

        System.out.println(items);
    }

    public CityAdapter(List<CityModel> infoCity) {
        this.infoCity = infoCity;
    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View tarjeta = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_city, viewGroup, false);
        return new ContactViewHolder(tarjeta);
    }


    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        final CityModel cityModel = infoCity.get(i);
        contactViewHolder.txtCity.setText(cityModel.getCity());
        contactViewHolder.txtContinente.setText(cityModel.getContinent());
        contactViewHolder.txtDescripcion.setText(cityModel.getDescription());
        contactViewHolder.txtPais.setText(cityModel.getCountry());

        contactViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(
                        v.getContext(),
                        "Ciudad:" + cityModel.getCountry() + cityModel.getCity(),
                        Toast.LENGTH_SHORT
                ).show();
            }
        });
    }



    @Override
    public int getItemCount() {
        return infoCity.size();
    }

    public void updateCityList(int position, String city) {
        infoCity.get(position).setCity(city);
        notifyItemChanged(position);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtCity;
        protected TextView txtContinente;
        protected TextView txtDescripcion;
        protected TextView txtPais;
        protected CardView cardView;

        static public int quantity = 0;

        static public int getQuantity() {
            return quantity;
        }

        static public void is() {
            System.out.print("Nothing fancy here");
        }

        static public void when() {
            System.out.print("Print Nothing");
        }

        public ContactViewHolder(View v) {
            super(v);
            txtCity = (TextView) v.findViewById(R.id.textViewCity);
            txtContinente = (TextView) v.findViewById(R.id.textViewContinente);
            txtDescripcion = (TextView) v.findViewById(R.id.textViewDescripcion);
            txtPais = (TextView) v.findViewById(R.id.textViewPais);
            cardView = (CardView) v.findViewById(R.id.cardView);
        }
    }
}
