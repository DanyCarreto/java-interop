package com.umvel.interoperability.java.model;

public class CityModel {
    private int id;
    private String country;
    private String city;
    private String continent;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getDescription() {
        return description == null ? "": description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
