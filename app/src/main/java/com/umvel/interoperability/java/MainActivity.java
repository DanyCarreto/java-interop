package com.umvel.interoperability.java;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.umvel.interoperability.R;
import com.umvel.interoperability.kotlin.CityKAdapter;
import com.umvel.interoperability.java.model.CityModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView listaTarjetas;
    FloatingActionButton floatingActionButton;
    CityModel berlin, newYork, tokyo, paris;
    List<CityModel> listCityModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_kotlin);

        listaTarjetas = (RecyclerView) findViewById(R.id.rvKotlinList);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);


        listaTarjetas.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listaTarjetas.setLayoutManager(llm);


        paris = new CityModel();
        paris.setCountry("Francia");
        paris.setCity("Paris");
        paris.setContinent("Europa");
        paris.setDescription("You wanna know how I got them?" +
                " So I had a wife. She was beautiful, like you, who tells me I worry too much," +
                " who tells me I ought to smile more, who gambles and gets in deep with the sharks. " +
                "Hey. One day they carve her face. And we have no money for surgeries.");

        berlin = new CityModel();
        berlin.setCountry("Alemania");
        berlin.setCity("Berlin");
        berlin.setContinent("Europa");
        berlin.setDescription("Because some men aren't looking for anything logical, like money. " +
                "They can't be bought, bullied, reasoned or negotiated with. Some men just wanna watch the world burn.");

        newYork = new CityModel();
        newYork.setCountry("New York");
        newYork.setCity("Estados Unidos de América");
        newYork.setContinent("América");
        newYork.setDescription("Let me get this straight. You think that your client, one of the wealthiest, " +
                "most powerful men in the world is secretly a vigilante who spends his nights beating criminals " +
                "to a pulp with his bare hands and your plan is to blackmail this person? Good luck.");

        tokyo = new CityModel();
        tokyo.setCountry("Japón");
        tokyo.setCity("Tokyo");
        tokyo.setContinent("Asia");
        tokyo.setDescription("Lorem Ipsum Pokemon,blahblahmanga, blahanime, pan blah blah");

        listCityModel = new ArrayList<>();
        listCityModel.add(paris);
        listCityModel.add(berlin);
        listCityModel.add(newYork);
        listCityModel.add(tokyo);
        listCityModel.add(paris);
        listCityModel.add(berlin);
        listCityModel.add(newYork);
        listCityModel.add(tokyo);

        CityKAdapter mAdapter = new CityKAdapter(listCityModel);
        listaTarjetas.setAdapter(mAdapter);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapter.getThisPosition();
                mAdapter.updateCityNameInPosition(2, "OtroJava");


                CityKAdapter.ContactViewHolder.Companion.returnQuantity();
                CityKAdapter.ContactViewHolder.Companion.setQuantity(2);
                CityKAdapter.ContactViewHolder.Companion.getQuantity();
                CityKAdapter.ContactViewHolder.returnStaticQuantity();
            }
        });

    }
}