package com.umvel.interoperability.kotlin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.umvel.interoperability.R
import com.umvel.interoperability.java.model.CityModel

class CityKAdapter(private val infoCity: List<CityModel?>) :
    RecyclerView.Adapter<CityKAdapter.ContactViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ContactViewHolder {
        val tarjeta: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_city, viewGroup, false)
        return ContactViewHolder(tarjeta)
    }

    override fun onBindViewHolder(contactViewHolder: ContactViewHolder, i: Int) {
        val cityModel = infoCity[i]
        contactViewHolder.txtCity.text = cityModel?.city
        contactViewHolder.txtContinente.text = cityModel?.continent
        contactViewHolder.txtDescripcion.text = cityModel?.description
        contactViewHolder.txtPais.text = cityModel?.country
        contactViewHolder.cardView.setOnClickListener(View.OnClickListener { v ->
            Toast.makeText(
                v.context,
                "tarjetas:" + cityModel?.country + cityModel?.city,
                Toast.LENGTH_SHORT
            ).show()
        })
    }


    override fun getItemCount(): Int {
        return infoCity.size
    }

    @get:JvmName("getThisPosition")
    @set:JvmName("setThisPosition")
    var position: Int = 0

    @JvmName("updateCityNameInPosition")
    fun updateCityList(position: Int, city: String?) {
        infoCity[position]?.city = city
        notifyItemChanged(position)
    }


    class ContactViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var txtCity: TextView
        var txtContinente: TextView
        var txtDescripcion: TextView
        var txtPais: TextView
        var cardView: CardView

        init {
            txtCity = v.findViewById<View>(R.id.textViewCity) as TextView
            txtContinente = v.findViewById<View>(R.id.textViewContinente) as TextView
            txtDescripcion = v.findViewById<View>(R.id.textViewDescripcion) as TextView
            txtPais = v.findViewById<View>(R.id.textViewPais) as TextView
            cardView = v.findViewById<View>(R.id.cardView) as CardView
        }

        companion object {
            var quantity: Int = 0
            fun returnQuantity(): Int {
                return quantity
            }
            @JvmStatic
            fun returnStaticQuantity(): Int {
                return 3
            }
        }
    }
}