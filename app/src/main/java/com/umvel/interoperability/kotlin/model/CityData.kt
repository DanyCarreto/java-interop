package com.umvel.interoperability.kotlin.model

data class CityData(
    val id: Int? = 0 ,
    val city: String,
    val country: String,
    val continent: String,
    val description: String?,
)
