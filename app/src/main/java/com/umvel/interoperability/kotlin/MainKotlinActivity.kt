package com.umvel.interoperability.kotlin

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.umvel.interoperability.java.CityAdapter
import com.umvel.interoperability.R
import com.umvel.interoperability.java.model.CityModel

class MainKotlinActivity : AppCompatActivity() {


    var listaTarjetas: RecyclerView? = null
    var berlin: CityModel? = null
    var newYork: CityModel? = null
    var tokyo: CityModel? = null
    var paris: CityModel? = null
    var listCityModel: ArrayList<CityModel?>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_kotlin)

        listaTarjetas = findViewById<View>(R.id.rvKotlinList) as RecyclerView
        listaTarjetas?.setHasFixedSize(true)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        listaTarjetas?.layoutManager = llm

        paris = CityModel()
        paris?.country = "Francia"
        paris?.city = "Paris"
        paris?.continent = "Europa"
        paris?.description = "You wanna know how I got them?" +
                " So I had a wife. She was beautiful, like you, who tells me I worry too much," +
                " who tells me I ought to smile more, who gambles and gets in deep with the sharks. " + "Hey. One day they carve her face. And we have no money for surgeries."

        berlin = CityModel()
        berlin?.country = "Alemania"
        berlin?.city = "Berlin"
        berlin?.continent = "Europa"
        berlin?.description =
            "Because some men aren't looking for anything logical, like money. " +
                    "They can't be bought, bullied, reasoned or negotiated with. Some men just wanna watch the world burn."

        newYork = CityModel()
        newYork?.country = "New York"
        newYork?.city = "Estados Unidos de América"
        newYork?.continent = "América"
        newYork?.description =
            "Let me get this straight. You think that your client, one of the wealthiest, " +
                    "most powerful men in the world is secretly a vigilante who spends his nights beating criminals " + "to a pulp with his bare hands and your plan is to blackmail this person? Good luck."

        tokyo = CityModel()
        tokyo?.country = "Japón"
        tokyo?.city = "Tokyo"
        tokyo?.continent = "Asia"
        tokyo?.description = "Lorem Ipsum Pokemon,blahblahmanga, blahanime, pan blah blah"


        listCityModel = ArrayList()
        listCityModel?.add(paris)
        listCityModel?.add(berlin)
        listCityModel?.add(newYork)
        listCityModel?.add(tokyo)
        listCityModel?.add(paris)
        listCityModel?.add(berlin)
        listCityModel?.add(newYork)
        listCityModel?.add(tokyo)


        val mAdapter = CityAdapter(listCityModel)
        mAdapter.itemCount
        listaTarjetas?.adapter = mAdapter

        val mAdapterk = CityKAdapter(listCityModel.orEmpty())
        mAdapterk.itemCount
        listaTarjetas?.adapter = mAdapterk


        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            mAdapterk.position
            mAdapter.position = 3
            mAdapter.updateCityList(2, "Otro")
            CityAdapter.ContactViewHolder.quantity = 2
            CityAdapter.ContactViewHolder.getQuantity()


            CityAdapter.ContactViewHolder.`is`()
            CityAdapter.ContactViewHolder.`when`()


            val array = intArrayOf(0, 1, 2, 3)
            mAdapter.addSomeCities(*array)
        }
    }
}