package com.umvel.interoperability.network

import android.content.Context
import com.umvel.network_android.networking.UmvelRetrofitNetworking
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkConfig {

    fun getRetrofit(
        context: Context
    ): Retrofit {
        return UmvelRetrofitNetworking.Builder(
            "https://www.urlbase.com",
            listOf(),
            12,
            12,
            12,
            true,
            context,
            true,
            GsonConverterFactory.create()
        )
            .interceptorList(listOf())
            .build()
    }
}